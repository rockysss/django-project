from django.contrib import admin

from .models import Question, Choice

class ChoiceInLine(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'pub_date', 'was_published_recently')

    fieldsets = [
        (None, {'fields' : ['question_text']}),
        ('Date Information', {'fields' : ['pub_date'], 'classes':['collapse'] }),
    ]
    list_filter = ['pub_date']
    inlines = [ChoiceInLine]
    search_fields = ['question_text']

    
    
    


# Register your models here.
admin.site.register(Question, QuestionAdmin)
# admin.site.register(Choice)